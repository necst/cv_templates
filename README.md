# CV templates

This repo contains two template CVs for:

* **Modern style CV**, to be built with ``make``
* **European CV**, to be build with ``make eu``

To clean intermediate files use ``make clean``, to clean everything you built use ``make distclean``.

## Modern style CV
It uses the class ``moderncv``and allows separate bibliographies, automatically filtered and printed into different sections, for which it uses ``biber``. Read comments in ``cv.tex`` for more details.

## European CV
It uses the ``europasscv`` class (a working copy from https://github.com/gmazzamuto/europasscv is included) and bibliographical entries are inserted manually into the text.
A possible feature addition is parsing bibliograpies as in the Modern style CV.
main := cv
nobib :=

.PHONY: clean distclean

LTC = pdflatex -interaction=nonstopmode -shell-escape -synctex=1
BIB := biber


all: final

final: ${main}.tex
	echo $(nobib)
#	for f in $(files); do aspell -t -p./$(main).dict -c $$f; done
	${LTC} ${main}.tex
ifneq ($(nobib),true)
	$(BIB) ${main}
endif
	${LTC} $(main).tex
	${LTC} $(main).tex
	${LTC} $(main).tex

cv: final

eu: eu_cv.tex
	make main=eu_cv nobib="true"

%.pdf: %.tex
	make final main=$(basename $<)

clean:
	@rm -f *.aux *.bbl *.blg *.dvi *.log *.out \
	*.ps *.synctex.gz *.blg *.xml *-blx.bib *.bcf &> /dev/null || true

distclean: clean
	@rm *.pdf &> /dev/null || true
